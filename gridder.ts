enum GridColor {
    UNCHANGED = "", WHITE = "white", BLACK = "black", RED = "red", BLUE = "blue", YELLOW = "yellow", GREEN = "green",
}

class Gridder {
    private gridSize: number;
    private gridData: GridColor[][];
    private gridOverlay: GridColor[][][];
    private currentColor: GridColor;
    private lastPosition: [number, number] | null;

    constructor(private canvas: HTMLCanvasElement, private colorPicker?: HTMLDivElement) { }

    public init(gridSize: number) {
        this.gridSize = gridSize;
        this.gridData = new Array(gridSize);
        this.gridOverlay = new Array(gridSize);
        for (var i = 0; i < this.gridSize; i++) {
            this.gridData[i] = new Array(gridSize);
            this.gridOverlay[i] = new Array(gridSize);
            for (var j = 0; j < this.gridSize; j++) {
                this.gridData[i][j] = GridColor.WHITE;
                this.gridOverlay[i][j] = [];
            }
        }
        this.currentColor = GridColor.BLACK;

        this.initCanvas();
        if (this.colorPicker) {
            this.initColorpicker();
        }
    }

    private initCanvas() {
        this.canvas.width = this.gridSize;
        this.canvas.height = this.gridSize;
        this.drawGrid();

        const setPixel = (evt) => {
            if (evt.target != this.canvas)
                return;
            const rect = this.canvas.getBoundingClientRect();
            const x = (evt.clientX - rect.left) / rect.width;
            const y = (evt.clientY - rect.top) / rect.height;
            this.processClick(x, y);
        };

        document.addEventListener("mousedown", (evt) => {
            setPixel(evt);
            this.canvas.addEventListener("mousemove", setPixel);
            document.addEventListener("mouseup", () => {
                this.canvas.removeEventListener("mousemove", setPixel);
                this.lastPosition = null;
            });
        });
    }

    private initColorpicker() {
        while (this.colorPicker.firstChild) {
            this.colorPicker.removeChild(this.colorPicker.firstChild);
        }

        for (let color in GridColor) {
            if (GridColor[color] == GridColor.UNCHANGED)
                continue;

            let elem = document.createElement("span");
            elem.style.display = "inline-block";
            elem.style.width = "10vh";
            elem.style.height = "10vh";
            elem.style.backgroundColor = color;
            elem.className = "colorsampler";
            this.colorPicker.appendChild(elem);

            elem.addEventListener("click", () => this.currentColor = GridColor[color]);
        }
    }

    public updateGrid(data: GridColor[][]) {
        for (var i = 0; i < this.gridSize; i++) {
            for (var j = 0; j < this.gridSize; j++) {
                this.updateTile(i, j, data[i][j]);
            }
        }
    }

    public updateTile(x: number, y: number, color: GridColor) {
        if (color != GridColor.UNCHANGED) {
            this.gridData[x][y] = color;
        }
        this.drawTile(x, y);
    }

    public drawGrid() {
        var ctx = this.canvas.getContext("2d");
        ctx.imageSmoothingEnabled = false;
        for (var x = 0; x < this.gridSize; x++) {
            for (var y = 0; y < this.gridSize; y++) {
                this.drawTile(x, y, ctx);
            }
        }
    }

    public drawTile(x: number, y: number, ctx?: CanvasRenderingContext2D) {
        if (!ctx) {
            ctx = this.canvas.getContext("2d");
        }

        if (this.gridOverlay[x][y].length > 0)
            ctx.fillStyle = <string> this.gridOverlay[x][y][this.gridOverlay[x][y].length-1];
        else
            ctx.fillStyle = <string> this.gridData[x][y];
        ctx.fillRect(x, y, 1, 1);
    }

    private processClick(x: number, y: number) {
        x *= this.gridSize;
        y *= this.gridSize;

        if (this.lastPosition) {
            let oldx = this.lastPosition[0];
            let oldy = this.lastPosition[1];
            let dx = x - oldx;
            let dy = y - oldy;
            let dist = Math.sqrt(dx * dx + dy * dy);

            for (var i = 1; i < dist; i++) {
                let tx = Math.floor(oldx + dx * (i / dist));
                let ty = Math.floor(oldy + dy * (i / dist));
                this.updateTile(tx, ty, this.currentColor);
            }
        }
        this.updateTile(Math.floor(x), Math.floor(y), this.currentColor);
        this.lastPosition = [x, y];
    }

    public distance(xFrom: number, yFrom: number, xTo: number, yTo: number) : Promise<number | null> {
        return Algs.BFS(xFrom, yFrom, xTo, yTo,
                       async (x, y) => !(x < 0 || x >= this.gridSize || y < 0 || y >= this.gridSize)
                       && this.gridData[x][y] == GridColor.WHITE,
                       (x, y) => null);
    }

    public async runAlg(alg: Algo) {
        const TIMEOUT = 2000. / this.gridSize / this.gridSize;
        function timeout(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        await alg(0, 0, this.gridSize - 1, this.gridSize - 1,
                async (x, y) => {
                    if (x < 0 || x >= this.gridSize
                        || y < 0 || y >= this.gridSize)
                        return false;
                    this.gridOverlay[x][y].push(GridColor.YELLOW);
                    this.drawTile(x, y);
                    await timeout(TIMEOUT);
                    this.gridOverlay[x][y].pop();
                    this.drawTile(x, y);
                    return this.gridData[x][y] == GridColor.WHITE;
                },
                async (x, y) => {
                    this.gridOverlay[x][y].push(GridColor.RED);
                    this.drawTile(x, y);
                    await timeout(TIMEOUT);
                });

        for (var i = 0; i < this.gridSize; i++) {
            for (var j = 0; j < this.gridSize; j++) {
                this.gridOverlay[i][j].length = 0;
            }
        }
        this.drawGrid();
    }
}

type Algo = (xFrom: number, yFrom: number, xTo: number, yTo: number,
          read: (x: number, y: number) => Promise<boolean>,
          write: (x: number, y: number) => Promise<void>) => Promise<number | null>;

let Algs = {
    "BFS": async function(xFrom, yFrom, xTo, yTo, read, write, dfs=false) : Promise<number | null> {
        let queue = [];
        let visited = {};
        let addNextIfAvailable = async (x, y, d) => {
            if (!await read(x, y))
                return;
            // TODO figure out a way to properly use Set with arrays
            const cacheEntry = x.toString() + " " + y.toString();
            if (visited[cacheEntry] <= d)
                return;
            visited[cacheEntry] = d;
            queue.push([x, y, d]);
        };
        await addNextIfAvailable(xFrom, yFrom, 0)
        while (queue.length > 0) {
            let x, y, d;
            if (dfs)
                [x, y, d] = queue.pop();
            else
                [x, y, d] = queue.shift();
            await write(x, y);
            if (x == xTo && y == yTo) {
                return d;
            }
            await addNextIfAvailable(x - 1, y, d + 1);
            await addNextIfAvailable(x + 1, y, d + 1);
            await addNextIfAvailable(x, y - 1, d + 1);
            await addNextIfAvailable(x, y + 1, d + 1);
        }
        return null;
    },
    "DFS": (xFrom, yFrom, xTo, yTo, read, write) => Algs.BFS(xFrom, yFrom, xTo, yTo, read, write, true),
};
